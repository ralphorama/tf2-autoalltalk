# [TF2] AutoAllTalk

Automatically enable `sv_alltalk` when there are fewer than a certain number of players playing.

**[Click here to download the plugin from alliedmods.net!](https://forums.alliedmods.net/showthread.php?p=2590467)**

AutoAllTalk is a simple plugin that enables `sv_alltalk` when your player count drops below a certain threshold. For example, if your threshold is 17, `sv_alltalk` will be `1` until there are 17 players in the server, at which point the plugin will set `sv_alltalk` to `0`. Once the player count drops back down to 16 or less, `sv_alltalk` will be set back to `1`.

## CVARS

- `sm_aat_threshold`: player count at which to disable alltalk.

## Dependencies

- (Dev only) [More Colors](https://forums.alliedmods.net/showthread.php?t=185016)

## Changelog

### 2018-05-03 (v1.0.1)

- Switched to using `GetConvarInt()` rather than `<convar>.IntValue`

### 2018-05-02 (v1.0.0)

- Initial creation
